<?php
/**
 * This source file is subject to the MIT License.
 * It is also available through http://opensource.org/licenses/MIT
 *
 * @category  Mereza
 * @package   Mereza_ERP
 * @copyright Copyright (c) 2011-2020 Romero Araujo. (romereza@gmail.com)
 * @author    Romero Araújo <romereza@gmail.com>
 * @license   http://opensource.org/licenses/MIT
 */
class Mereza_ERP_Helper_Data extends Mage_Core_Helper_Abstract {

}
