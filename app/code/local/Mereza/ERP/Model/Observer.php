<?php
/**
 * This source file is subject to the MIT License.
 * It is also available through http://opensource.org/licenses/MIT
 *
 * @category  Mereza
 * @package   Mereza_ERP
 * @copyright Copyright (c) 2011-2020 Romero Araujo. (romereza@gmail.com)
 * @author    Romero Araújo <romereza@gmail.com>
 * @license   http://opensource.org/licenses/MIT
 */
class Mereza_ERP_Model_Observer
{

	/**
	 * Exportar pedido para o ERP
	 *
	 * @param Varien_Event_Observer $observer
	 * @return void
	 */
	public function send($observer) {
		try{

			$active=Mage::getStoreConfig("mereza_erp/general/active"); 
			$endpoint=Mage::getStoreConfig("mereza_erp/general/endpoint"); 
			$apikey=Mage::getStoreConfig("mereza_erp/general/apikey"); 
			$addLog=Mage::getStoreConfig("mereza_erp/general/addLog"); 
			$action=Mage::getStoreConfig("mereza_erp/general/action"); 

			if (!$active) {
				return false;
			}

			$order = $observer->getEvent()->getOrder();
			$payment = $order->getPayment();
			$itens = $order->getAllItems();
			// Zend_Debug::dump($order->getAllItems());exit;
			Mage::log($order->debug(), null, "order.log", true);

			$customerBd = Mage::getModel("customer/customer")->load($order->getCustomerId());

			$shippingAddress = $order->getShippingAddress();
			$address = $shippingAddress->getStreet();
			$ponto_ref = $shippingAddress->getPontoRef();

			$tipopessoa = $customerBd->getTipopessoa();

			$fone = (trim($shippingAddress->getTelephone())!="")? $shippingAddress->getTelephone():$customerBd->getTelephone();
			$fax = (trim($shippingAddress->getFax())!="")? $shippingAddress->getFax():$customerBd->getFax();
		
			// Zend_Debug::dump($customerBd);
			// Zend_Debug::dump($shippingAddress);
			// Zend_Debug::dump($address);
			// Zend_Debug::dump($fone);
			// Zend_Debug::dump($fax);
			// Zend_Debug::dump($order);
			
			// exit;

			if ($tipopessoa == "3") {
				$customer = [
					"name" => $customerBd->getFirstname(),
					"cpf_cnpj" => $customerBd->getTaxvat(),
					"telephone" => $fone,
					"dob" => $customerBd->getDob()
				];
		
			}else{
				$customer = [
					"name" => $customerBd->getFirstname(),
					"telephone" => $fone,
					"cnpj" => $customerBd->getTaxvat(),
					"razao_social" => $customerBd->getFirstname(),
					"nome_fantasia" => $customerBd->getLastname(),
					"ie" => $customerBd->getIe(),
				];
			}

			$itens_arr = [];
			foreach ($itens as $key => $value) {
				$itens_arr[] = [
					"sku" => $value->getSku(),
					"name" => htmlentities($value->getName()),
					"price" => $value->getPrice(),
					"qty" => $value->getQtyOrdered(),
				];
			}


			$obj = [
				"customer" => $customer,

				"shipping_address" => [
					"street" => $address["0"],
					"number" => $address["1"],
					"additional" => $address["2"],
					"neighborhood" => $address["3"],
					"city" => $shippingAddress->getCity(),
					"city_ibge_code" => null,
					"uf" => $shippingAddress->getRegion(),
					"country" => $shippingAddress->getCountryId(),
				],

				"items" => $itens_arr,

				"shipping_method" => $order->getShippingMethod(),
				"payment_method" => $payment->getMethod(),
				// "payment_installments" => "Número de parcelas (opcional)",
				"subtotal" => $order->getBaseSubtotal(),
				"shipping_amount" => $order->getShippingAmount(),
				"discount" => abs($order->getDiscountAmount()),
				"total" =>  $order->getBaseGrandTotal(),
			];

			$json = json_encode($obj);
			// echo $json;
			// Zend_Debug::dump($json);

			$client = new Zend_Http_Client();
			$client->setUri($endpoint.$action);

			$client->setHeaders("Authorization", "Bearer ".$apikey);
			$response = $client->setRawData($json)->setEncType("application/json")->request("POST");

			if ($response->getStatus() == 200) {
				Mage::log("Dados enviados com sucesso para o ERP", null, "Mereza_ERP.log");
				Mage::log($json, null, "Mereza_ERP.log");
				Mage::log("######################", null, "Mereza_ERP.log");
			} else {
				Mage::log("ERRO ao enviar pedidos para o ERP", null, "Mereza_ERP.log");
				Mage::log("Status: ".$response->getStatus(), null, "Mereza_ERP.log");
				Mage::log("Mensagem: ".$response->getMessage(), null, "Mereza_ERP.log");
				Mage::log($json, null, "Mereza_ERP.log");
				Mage::log("######################", null, "Mereza_ERP.log");
			}

			// exit;
		} catch (Exception $ex) {
			Mage::log("ERRO ao enviar pedidos para o ERP", null, "Mereza_ERP.log");
			Mage::log("Mensagem: ".$ex->getMessage(), null, "Mereza_ERP.log");
			Mage::log("######################", null, "Mereza_ERP.log");

			Mage::getSingleton("core/session")->addError(Mage::helper("mereza_erp")->__($ex->getMessage()));

			// exit;
		}
	}
}